#import "rational.typ" : *

#let _do-operation(rows, op) = {
    if op.at(0) == "swap" {
        let a = op.at(1)
        let b = op.at(2)

        if a > b {
            let a-row = rows.remove(a - 1)
            let b-row = rows.remove(b - 1)

            rows.insert(b - 1, a-row)
            rows.insert(a - 1, b-row)
        } else {
            let b-row = rows.remove(b - 1)
            let a-row = rows.remove(a - 1)

            rows.insert(a - 1, b-row)
            rows.insert(b - 1, a-row)
        }

        (rows, $"Swap" r_#a "and" r_#b$)
    } else if op.at(0) == "scale" {
        let idx = op.at(1)
        let scale = op.at(2)

        let row = rows.remove(idx - 1)

        row = row.map(v => mul(v, scale))

        rows.insert(idx - 1, row)

        (rows, $"Scale" r_#idx "by" #display(scale)$)
    } else if op.at(0) == "add" {
        let first = op.at(1)
        let second = op.at(2)
        let third = op.at(3, default: none)

        let from
        let to
        let scale

        if third != none {
            scale = first
            from = second
            to = third
        } else {
            scale = 1
            from = first
            to = second
        }

        let adding-row = rows.at(from - 1)

        let row = rows.remove(to - 1)

        row = row.zip(adding-row).map(((x, y)) => add(x, mul(y, scale)))

        rows.insert(to - 1, row)
        
        (
            rows,
            if scale == 1 {
                $"Add" r_#from "to" r_#to$
            } else {
                $"Add" #display(scale) dot r_#from "to" r_#to$
            }
        )
    } else if op == "normalize" {
        (
            rows.map(row => {
                let scale-down-by = row.find(n => n != 0)

                if scale-down-by == none {
                    row
                } else {
                    row.map(n => div(n, scale-down-by))
                }
            }),
            [Normalize the matrix]
        )
    }
}

#let _matrix(options, rows) = {
    math.equation(math.mat(..options, ..rows.map(v => v.map(display))), block: true)
}

/// Perform row operations on a matrix
///
/// ```examplec
/// gaussian-elimination(
///     operations: (
///         ("swap", 1, 2),
///         ("add", -3, 1, 2),
///         ("add", 2, 1),
///         ("scale", 2, -1),
///         "normalize",
///     ),
///     options: (augment: -1),
///     (3, 4, (5, 6)),
///     (1, 2, (1, 2)),
/// )
/// ```
/// -> content
#let gaussian-elimination(
    /// A list of row operations, see the example for which ones are available -> array
    operations: (),
    /// A list of options to be passed through to the `mat` function -> dictionary
    options: (),
    /// A list of numbers defining the starting matrix, as per the `rational` module -> array
    ..rows,
) = {
    let rows = rows.pos()

    let elements = (_matrix(options, rows), []) + for operation in operations {
        let (new-rows, what) = _do-operation(rows, operation)

        rows = new-rows
    
        (_matrix(options, rows), align(horizon, what))
    }

    align(center, grid(columns: 2, gutter: 1em, ..elements))
}

#let Proj = math.op("Proj")

/// Perform vector projection
///
/// ```example
/// $
///     Proj_b (a) = (a dot b)/(b dot b) dot b
///         = #do-proj((1, (1, 2), 3), (1, 1, 1))
/// $
/// ```
/// -> content
#let do-proj(
    /// A list of numbers as per the `rational` module; represents the vector to project. -> array
    a,
    /// A list of numbers as per the `rational` module; represents the vector to project onto. -> array
    b,
) = {
    let sum_top = a.zip(b).map(((a, b)) => $#display(a) dot #display(b)$).join($+$)
    let sum_bottom = b.zip(b).map(((a, b)) => $#display(a) dot #display(b)$).join($+$)

    let sum_top_val = a.zip(b).map(((a, b)) => mul(a, b)).fold(0, (a, v) => add(a, v))
    let sum_bottom_val = b.zip(b).map(((a, b)) => mul(a, b)).fold(0, (a, v) => add(a, v))

    let frac_result = div(sum_top_val, sum_bottom_val)

    let result = b.map(b => mul(b, frac_result))

    let bvec = math.vec(..b.map(b => display(b)))
    let resvec = math.vec(..result.map(v => display(v)))

    $#sum_top/#sum_bottom bvec = #display(frac_result) bvec = resvec$
}

