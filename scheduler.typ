#import "@preview/cetz:0.3.2"

/// Set the size of your text to make it fit snugly inside of a box. Must be called with context.
///
/// ```example
/// // The style is required to measure the text size
/// #context {
///     // You can pass a single string or a list to ensure every element of the string fits while having the same size
///     let fitted = fit(("A", "AB", "ABC"), 20pt, 20pt)
///         .map(text => box( // Draw an outline
///             text,
///             stroke: 1pt,
///             width: 100%,
///             height: 100%
///         ))
///
///     grid(
///         columns: (20pt,),
///         rows: 20pt,
///         ..fitted,
///     )
/// }
/// ```
///
#let fit(
    /// The string of text to fit in the box, or an array of text where each text item will be the same size and fit in the box -> string | array
    values,
    /// The width of the box -> length
    width,
    /// The height of the box -> length
    height,
    /// A list of options to be passed through to the `text` constructor -> dictionary
    options: (),
) = {
    let to-one = false

    if type(values) != array {
        values = (values,)
        to-one = true
    }

    // Binary search to narrow in on a good size
    let max-size = 100in
    let min-size = 0in

    for _ in range(0, 100) {
        let mid = (max-size + min-size) / 2

        let okay = true

        for value in values {
            let size = measure(text(size: mid, [#value]))

            if size.width > width or size.height > height {
                max-size = mid
                okay = false
            }
        }

        if okay {
            min-size = mid
        }
    }

    let texts = values.map(value => text(size: min-size, [#value], ..options))

    if to-one {
        texts.at(0)
    } else {
        texts
    }
}

#let _draw_schedule(start, end, days, items, size) = {
    if start > 23 or start < 0 {
        panic("Start must be between 0 and 23")
    }

    if end > 23 or end < 0 {
        panic("End must be between 0 and 23")
    }

    let hour_count = calc.rem-euclid(end - start, 24)

    let width = size.width
    let height = size.height

    let hour_height = height / (hour_count + 1)
    let day_width = width / (days.len() + 1/2)
    let day_offset = day_width / 2

    cetz.canvas({
        import cetz.draw: *

        set-style(line: (stroke: (thickness: 1.2pt)))

        // Make sure there's space at the start even if nothing else is drawn there
        circle((0, 0), stroke: none, fill: none, radius: 0)

        let days_fitted = fit(days, day_width * 0.8, hour_height * 0.5)

        for (day, idx) in days_fitted.zip(range(0, days_fitted.len())) {
            content((day_offset + day_width * (0.5 + idx), height - hour_height / 2), anchor: "center", day)
        }

        set-style(line: (stroke: (paint: black, dash: none)))

        let hours = fit(
            range(0, hour_count).map(idx => calc.rem-euclid(idx + start, 24)),
            day_offset * 0.7,
            hour_height * 0.35,
        )

        for (idx, hour) in range(0, hour_count).zip(hours) {
            line((day_offset, height - hour_height * (1 + idx)), (width, height - hour_height * (1 + idx)))
            set-style(line: (stroke: (paint: gray, dash: "dotted"))) // janky way to make just the first line solid
            content((day_offset * 0.8, height - hour_height * (1 + idx)), hour, anchor: "east")
        }

        set-style(line: (stroke: (paint: black, dash: none)))

        for idx in range(0, days.len() + 1) {
            line((day_offset + day_width * idx, 0), (day_offset + day_width * idx, height))
        }

        for item in items {
            let hour_idx = calc.rem-euclid(item.start - start, 24)
            let duration = item.at("duration", default: 0.)
            let label-text = item.label
            let subtext = item.at("subtext", default: "")
            let options = item.at("options", default: ())

            let draw_day(x, y, h, w, half) = {
                line((x, y), (x + w * if half { 0.5 } else { 1 }, y), stroke: (thickness: 2pt, dash: if options.contains("fuzzy start") { "dashed" } else { "solid" }))

                line((x, y - h), (x + w * if half { 0.5 } else { 1 }, y - h), stroke: (thickness: 2pt, dash: if options.contains("fuzzy end") { "dashed" } else { "solid" }))

                let offset = calc.max(calc.min(calc.min(day_width, w) * 0.1, calc.min(hour_height, duration * hour_height) * 0.1), 3pt)

                content((x + offset, y - offset), anchor: "north-west", fit(label-text, w - offset * 2, calc.min(0.3 * hour_height, duration * hour_height - offset * 2)))

                content((x + w - offset, y - calc.min(h, 1.5 * hour_height) + offset), anchor: "south-east", fit(subtext, w - (offset * 2), calc.min(0.2 * hour_height, duration * hour_height - offset * 2)))
            }

            for day in item.days {
                let x = day_offset + day_width * days.position(v => v == day)
                let y = height - hour_height * (1 + hour_idx)

                let optional = options.contains("optional")
                let left = optional and options.contains("left")
                let right = optional and options.contains("right")

                draw_day(x + if right { day_width * 0.5 } else { 0in }, y, duration * hour_height, day_width * if left or right { 0.5 } else { 1. }, optional and not (left or right))
            }
        }
    })
}

/// Lets you make super fancy schedules; operates on 24 hour time
///
/// ```example
/// #let sched = schedule(
///     14,
///     16,
///     ("M", "T"),
///     (
///         (
///             start: 14.5,
///             duration: 1,
///             label: "Class",
///             subtext: "Location",
///             days: ("M", "T"),
///             options: ("fuzzy end",),
///         ),
///     )
/// )
///
/// // The schedule will fill the space you give it
/// #box(
///     width: 2in,
///     height: 2in,
///     sched
/// )
/// ```
///
#let schedule(
    /// Start time in hours -> float
    start,
    /// End time in hours -> float
    end,
    /// A list of column headers as strings, can be any length for any number of columns. Intended to represent weekdays. -> array
    days,
    /// An array of schedule items, each item is a dictionary with the following items:
    /// - `start`: The start time in hours
    /// - `duration`: The duration of the event in hours (default: `0`)
    /// - `label`: The label to go at the top of the item (default: `""`),
    /// - `subtext`: Subtext to go at the bottom of the item (default: `""`)
    /// - `days`: An array of column headers for when the item takes place
    /// - `options`: An array of options to do with the item, includes any of the following:
    ///
    /// #table(
    ///     columns: 2,
    ///     [`"fuzzy start"`], [Renders the start line as dashed, intended to represent the start time being fuzzy],
    ///     [`"fuzzy end"`], [Renders the end line as dashed, intended to represent the end time being fuzzy],
    ///     [`"optional"`], [Makes the start and end lines take half the width of the column, intended to represent the event being optional],
    ///     [`"left"`], [Must be accompanied with `optional`; forces the entire event to take up the left half of the column],
    ///     [`"right"`], [Must be accompanied with `optional`; forces the entire event to take up the right half of the column]
    /// ) -> array
    items,
) = {
    context {
        layout(size => {
            _draw_schedule(start, end, days, items, size)
        })
    }
}

