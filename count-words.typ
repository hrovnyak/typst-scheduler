#let _word_count = state("word-count", 0)

/// A function intended to go in a `show` statement that will begin counting words
///
/// Goes along with @word-count
///
/// ```example
/// #show: count-words
///
/// = A B C
///
/// a-b c'd // - binds words into one, having ' bind words doesn't really work because of smartquote
///
/// #word-count()
/// ```
#let count-words(body) = {
    show regex("\\w+[\\w-]*"): v => {
        _word_count.update(v => v + 1)

        v
    }

    body
}

/// Insert the current word count into the document
///
/// Goes along with @count-words
#let word-count() = _word_count.display()
