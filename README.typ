#import "lib.typ" : *
#import "@preview/tidy:0.4.1"

#show link: set text(fill: blue)
#show link: underline

= `henrys-typst-utils`

== What is this?

A bunch of random scripts to do miscellaneous fancy things

== How do I install this?

```bash
mkdir {data-dir}/typst/packages/local/henrys-typst-utils
cd {data-dir}/typst/packages/local/henrys-typst-utils
git clone https://gitlab.com/hrovnyak/henrys-typst-utils 0.1.0
```

In typst:

```typst
#import "@local/henrys-typst-utils@0.1.0" : *
```

Reference the instructions here to find your local data folder: #link("https://github.com/typst/packages#local-packages")

== What are the functions here?

Good question!

=== Modules

- #link(<utils>, [henrys-typst-utils])
- #link(<word-counting>, [Word counting])
- #link(<rational>, [Rationals])
- #link(<code-theme>, [Code theme])

#let lib-docs = tidy.parse-module(read("lib.typ"), scope: (arrow: arrow, list-markers: list-markers, yeah: yeah))

#let truth-table-docs = tidy.parse-module(read("truth-table.typ"), scope: (impl: impl, truth-table: truth-table, pagenum: pagenum))

#let schedule-docs = tidy.parse-module(read("scheduler.typ"), scope: (schedule: schedule, fit: fit))

#let gaussian-elimination-docs = tidy.parse-module(read("gaussian-elimination.typ"), scope: (gaussian-elimination: gaussian-elimination, do-proj: do-proj, Proj: Proj))

#let pinner-docs = tidy.parse-module(read("pinner.typ"), scope: (pins: pins))

#let modules = (lib-docs, truth-table-docs, schedule-docs, gaussian-elimination-docs, pinner-docs)

= Utils

#tidy.show-module((
    name: "henrys-typst-utils",
    functions: modules.map(v => v.functions).flatten(),
    label-prefix: "",
    scope: modules.map(v => v.scope).join(),
    variables: modules.map(v => v.variables).flatten(),
    preamble: "",
)) <utils>

#let count-words-docs = tidy.parse-module(read("count-words.typ"), scope: (count-words: count-words, word-count: word-count), name: "Word counting")

= Word counting

#tidy.show-module(count-words-docs) <word-counting>

#let rational-docs = tidy.parse-module(read("rational.typ"), scope: (neg: neg, recip: recip, add: add, sub: sub, mul: mul, div: div, display: display), name: "rational")

= Rationals

<rational>

The `rational` module provides utilities for working with integers, fractions, and floats. It provides operators that will take input one of those three types, upcast them to the minimum precision needed, perform the operation, and simplify the result.

Fractions are represented as an array of the form `(*numerator*, *denominator*)`, so $1/2$ would be represented as `(1, 2)`.

Simplification:
- `(2, 4)` will be simplified to `(1, 2)`
- `(2, 1)` will be simplified to `2`
- `2.0` will be simplified to `2`

#tidy.show-module(rational-docs)

#let code-theme-docs = tidy.parse-module(read("code-theme.typ"), scope: (code-theme: code-theme))

= Code theme

#tidy.show-module(code-theme-docs) <code-theme>
