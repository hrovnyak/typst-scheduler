/// Evaluates the implication connective
///
/// ```examplec
/// truth-table(
///     ("p", "q"),
///     (
///         ($p -> q$, (p, q) => impl(p, q)),
///     )
/// )
/// ```
///
/// -> boolean
#let impl(
    /// Left hand side -> boolean
    a,
    /// Right hand side -> boolean
    b,
) = {
    not a or b
}

#let _truth-table(variables, values, columns) = {
    if variables.len() != 0 {
        let new_vars = variables.slice(1)
        
        return _truth-table(new_vars, values + (true,), columns) + _truth-table(new_vars, values + (false,), columns)
    }

    let data = for value in values {
        (if value [T] else [F],)
    }

    data = data + for column in columns {
        let value = column.at(1)(..values)
        (if value [T] else [F],)
    }

    return data
}

/// Creates a truth table
///
/// ```examplec
/// truth-table(
///     ($p$, $q$, $r$),
///     (
///         (
///             $p -> q$,
///             (p, q, r) => impl(p, q)
///         ),
///         (
///             $(p -> q and q -> r) -> (p -> r)$,
///             (p, q, r) => impl(impl(p, q) and impl(q, r), impl(p, r))
///         ),
///     )
/// )
/// ```
///
/// -> content
#let truth-table(
    /// A content array for each variable to be included in the table -> array
    variables,
    /// An array of columns; each column is an array where the first element is the content to be displayed at the top of the column and the second is a predicate taking in each of the variables and returning a boolean -> array
    columns,
) = {
    let data = for var in variables {
        (var,)
    }

    data = data + for col in columns {
        (col.at(0),)
    }

    data = data + _truth-table(variables, (), columns)

    table(
        columns: variables.len() + columns.len(),
        rows: calc.pow(2, variables.len()),
        ..data
    )
}
