#import "truth-table.typ" : *
#import "scheduler.typ" : *
#import "count-words.typ" : *
#import "rational.typ" : *
#import "gaussian-elimination.typ" : *
#import "code-theme.typ"
#import "pinner.typ" : *

/// Display a harpoon arrow on top of a piece of content to represent a vector
///
/// #example(`$arrow(x)$`)
#let arrow(
    /// The content to draw an arrow above -> content
    x
) = $harpoon.rt(#x)$

/// Set list markers to alternate between dot and dash
///
/// #example(`
/// #show list: list-markers
///
/// - A
///     - B
///         - C
/// `, mode: "markup")
#let list-markers(it) = {
    set list(marker: n => if calc.rem(n, 2) == 0 [#sym.bullet] else [--])
    it
}

// From svg-emoji code

/// Yeah! (From the Purdue Hackers discord)
///
/// #example(`
/// #yeah
///`, mode: "markup")
#let yeah = context {
    let h = measure([X]).height
    box(
        align(horizon, image("yeah.png", height: 1em, alt: "Yeah!")),
        height: h,
        outset: (y: (1em- h) / 2),
    )
}

/// Document which page a piece of information comes from
#let pagenum(
    /// The page number -> number
    num,
    /// Page offset between how they're shown in the PDF viewer vs. in the document. Maybe the page listed in your pdf viewer is different from the page shown in the document -> number
    offset: 0,
) = {
    if offset == 0 {
        box(align(right, [$#sym.arrow.b$ from page #num]), width: 1fr)
    } else {
        box(align(right, [$#sym.arrow.b$ from page #num $->$ #(num+offset)]), width: 1fr)
    }
}

#let Nul = math.op("Nul")
#let Span = math.op("Span")
#let Trace = math.op("Trace")
