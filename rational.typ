#let num-set(v) = if type(v) == int {
    "Z"
} else if type(v) == array and v.len() == 2 and type(v.at(0)) == int and type(v.at(1)) == int {
    "Q"
} else if type(v) == float {
    "R"
} else {
    panic("Not a number: ", v)
}

#let _upcast(v, to) = {
    let from = num-set(v)
    
    if from == to {
        return v
    }

    if from == "Z" and to == "Q" {
        (v, 1)
    } else if from == "Z" and to == "R" {
        float(v)
    } else if from == "Q" and to == "R" {
        float(v.at(0)) / float(v.at(1))
    } else {
        none
    }
}

#let _upcast-to-match(a, b) = {
    let a-set = num-set(a)
    let b-set = num-set(b)

    let a-upcasted = _upcast(a, b-set)

    if a-upcasted != none {
        return (a-upcasted, b, b-set)
    }

    let b-upcasted = _upcast(b, a-set)

    if b-upcasted != none {
        return (a, b-upcasted, a-set)
    }

    panic("Failed to match the sets: ", a, b)
}

#let _simplify(v) = {
    let ty = num-set(v)

    if ty == "Z" {
        v
    } else if ty == "Q" and v.at(1) == 1 {
        v.at(0)
    } else if ty == "Q" {
        let (q1, q2) = v

        if q2 < 0 {
            q1 = -q1
            q2 = -q2
        }

        let gcd = calc.gcd(q1, q2)

        let new-v = (calc.quo(q1, gcd), calc.quo(q2, gcd))

        if new-v.at(1) == 1 {
            new-v.at(0)
        } else {
            new-v
        }
    } else if ty == "R" and calc.round(v) == v {
        int(v)
    } else if ty == "R" {
        v
    }
}

/// Negates a number
///
/// ```examplec
/// neg((1, 2))
/// ```
///
/// -> number
#let neg(
    /// The number to be negated -> number
    q
) = {
    let ty = num-set(q)

    _simplify(if ty == "Z" or ty == "R" {
        -q
    } else {
        (-q.at(0), q.at(1))
    })
}

/// Takes the reciprocal of a number
///
/// ```examplec
/// recip(2)
/// ```
///
/// -> number
#let recip(
    /// The number to be reciprocated -> number
    q
) = {
    let ty = num-set(q)

    _simplify(if ty == "Z" {
        (1, q)
    } else if ty == "Q" {
        (q.at(1), q.at(0))
    } else if ty == "R" {
        1 / q
    })
}

/// Adds two numbers
///
/// ```examplec
/// add((1, 2), 0.333)
/// ```
///
/// -> number
#let add(
    /// The left hand side -> number
    a,
    /// The right hand side -> number
    b
) = {
    let (a, b, ty) = _upcast-to-match(a, b)

    _simplify(if ty == "Z" or ty == "R" {
        a + b
    } else if ty == "Q" {
        let (a1, a2) = a
        let (b1, b2) = b

        (a1 * b2 + b1 * a2, a2 * b2)
    })
}

/// Subtracts two numbers
///
/// ```examplec
/// sub((1, 2), 0.333)
/// ```
///
/// -> number
#let sub(
    /// The left hand side -> number
    a,
    /// The right hand side -> number
    b,
) = add(a, neg(b))

/// Multiplies two numbers
///
/// ```examplec
/// mul((1, 2), (2, 1))
/// ```
///
/// -> number
#let mul(
    /// The left hand side -> number
    a,
    /// The right hand side -> number
    b,
) = {
    let (a, b, ty) = _upcast-to-match(a, b)

    _simplify(if ty == "Z" or ty == "R" {
        a * b
    } else if ty == "Q" {
        let (a1, a2) = a
        let (b1, b2) = b

        (a1 * b1, a2 * b2)
    })
}

/// Divides two numbers
///
/// ```examplec
/// div((1, 2), (2, 1))
/// ```
///
/// -> number
#let div(
    /// The left hand side -> number
    a,
    /// The right hand side -> number
    b,
) = mul(a, recip(b))

/// Displays a number as content
///
/// ```examplec
/// display((1, 2))
/// ```
/// ```examplec
/// display(9)
/// ```
/// ```examplec
/// display(1.666666666)
/// ```
///
/// -> content
#let display(
    /// The number to be displayed -> number
    v
) = {
    let v = _simplify(v)

    let ty = num-set(v)

    if ty == "Z" {
        $#v$
    } else if ty == "Q" {
        let (q1, q2) = v

        let neg = if q1 < 0 {
            $-$
        } else {
            $$
        }
    
        $#neg #calc.abs(q1) / #q2$
    } else if ty == "R" {
        $#(calc.round(v, digits: 2))$
    }
}
