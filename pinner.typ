#let _circle(total-radius, margin, debug, pin) = {
    let content = pin.at("content", default: [])

    let rotation = pin.at("rotate", default: 0deg)
    let scaling = pin.at("scale", default: 100%)
    let movement = pin.at("move", default: (0pt, 0pt))
    let background = pin.at("background", default: none)

    let (scale-x, scale-y) = if type(scaling) == array {
        scaling
    } else {
        (scaling, scaling)
    }
    
    place(
        center + horizon,
        box(
            clip: true,
            width: total-radius * 2,
            height: total-radius * 2,
            radius: total-radius,
            fill: background,
            move(
                dx: movement.at(0),
                dy: -movement.at(1),
                rotate(
                    rotation,
                    scale(x: scale-x, y: scale-y, content),
                ),
            )
        )
    )

    if debug {
        place(center + horizon, circle(radius: total-radius - margin / 2, stroke: (
        paint: black.transparentize(50%), thickness: margin)))
    }

    circle(radius: total-radius, stroke: black)
}

// Calculate the optimal way to place the pins given the container size
#let _calc-positions(container-size, pin-radius) = {
    let max-width = calc.floor(container-size.width / (pin-radius * 2))
    let grid-height = calc.floor(container-size.height / (pin-radius * 2))

    let grid-count = max-width * grid-height

    let dense-count = 0

    let dense-alternate-width = calc.floor((container-size.width - pin-radius) / (pin-radius * 2))

    let dense-row-height = pin-radius * 2 * calc.cos(30deg)

    let dense-height = calc.floor(container-size.height / dense-row-height)

    let section-length = max-width + dense-alternate-width

    let dense-count = section-length * calc.floor(dense-height / 2) + max-width * calc.rem-euclid(dense-height, 2)

    if grid-count > dense-count {
        range(grid-count).map(i => (
            x: (calc.rem-euclid(i, max-width)) * pin-radius * 2,
            y: calc.floor(i / max-width) * pin-radius * 2
        ))
    } else {
        range(dense-count).map(i => {
            let section = calc.floor(i / section-length)
            let section-pos = i - section * section-length

            if section-pos < max-width {
                (
                    x: section-pos * pin-radius * 2,
                    y: 2 * section * dense-row-height
                )
            } else {
                (
                    x: (section-pos - max-width) * pin-radius * 2 + pin-radius,
                    y: (2 * section + 1) * dense-row-height
                )
            }
        })
    }
}

/// Generate a printout to make pins using a pin press.
/// 
/// This function will automatically arrange the pins within the space provided to maximize the amount of pins on the page. It will choose either a square or hexagonal grid depending on which one fits more pins.
///
/// Recommended to #raw("set page(margin: 0pt)", lang: "typc") to fit more pins.
///
/// ```example
/// #box(
///     width: 2in,
///     height: 2in,
///     pins(
///         content-width: 0.4in,
///         margin: 0.1in,
///         pins: (
///             (
///                 content: [Some image],
///                 scale: 70%,
///             ),
///         )
///    )
/// )
/// ```
///
/// - content-width (length): The diameter of the visible area of the pin
/// - margin (length): The extra margin needed by the pin press. If given an inner and outer diameter, be sure to divide by two.
/// - debug (boolean): Shade the margin area to show what content will be visible. It is recommended to turn this off when making a final printout to avoid the margin appearing darker on the pin.
#let pins(
    /// The diameter of the visible area of the pin -> length
    content-width: 1.5in,
    /// The extra margin needed by the pin press. If given an inner and outer diameter, be sure to divide by two. -> length
    margin: 0.125in,
    /// Shade the margin area to show what content will be visible. It is recommended to turn this off when making a final printout to avoid the margin appearing darker on the pin. -> boolean
    debug: true,
    /// - An array of dictionaries containing any or none of the following options:
    ///     - `content` (`content`): The content to display inside the pin
    ///     - `rotate` (`angle`): How much to rotate the content
    ///     - `scale` (`ratio`, `array`): How much to scale the content. Either a `ratio` or an `array` of two `ratio`s for `x` and `y` scaling
    ///     - `move` (`array`): An `array` of two `length`s for `x` and `y` translation. -> array
    pins: ()
) = {
    let total-radius = content-width / 2 + margin
    let dense-scale = calc.cos(30deg)

    layout(size => {
        let positions = _calc-positions(size, total-radius)
    
        box(
            width: size.width,
            height: size.height,
            for i in range(positions.len()) {
                let pin = pins.at(i, default: (empty: ()))
        
                place(left,
                    dx: positions.at(i).x,
                    dy: positions.at(i).y,
                    _circle(total-radius, margin, debug, pin)
                )
            }
        )       
    })

}

