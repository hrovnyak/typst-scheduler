#import "@preview/touying:0.5.5": *

#let slide(..args) = touying-slide-wrapper(self => {
  let has-title = self.headings.at(0, default: (:)).at("label", default: "") != <touying:hidden>

  // set page
  let header(self) = {
    pad(
      left: 2em,
      top: 2em,
      {
      // [#set text(5pt); #set par(leading: 0.1em); #self.pairs().slice(24)]
        if has-title {
          set text(size: 30pt)
          let heading = utils.display-current-heading(level: 2)
          smallcaps(strong(heading))

          block(line(start: (0pt, 0pt), length: 100%, stroke: self.colors.tertiary), width: 20%, above: 0.6em, below: 0.8em)
        }
      }
    )
  }

  self = utils.merge-dicts(
    self,
    config-page(
      header: header,
      footer: none,
      margin: (
        top: if has-title { 6em } else { 2em },
        rest: 2em,
      ),
    ),
  )

  set text(if args.pos().len() > 1 { 15pt } else { 20pt })

  touying-slide(self: self, ..args.named(), ..args.pos().enumerate().map(((i, v)) => {
    if i == 0 {
      v
    } else {
      place(top, line(start: (0.1em, 0pt), length: 100%, angle: 90deg, stroke: self.colors.secondary), dx: .5em)

      block(v, inset: (left: 2em))
    }
  }))
})

#let title-slide(
  title,
  author,
  note: [],
) = touying-slide-wrapper(self => {
  let body = {
    set align(center + horizon)

    smallcaps(strong(title))
    block(line(start: (0pt, 0pt), length: 100%, stroke: self.colors.tertiary), width: 20%, above: 0.8em, below: 0.7em)
    text(size: .7em, author, font: "Monaspace Radon Var")

    place(top+left, polygon(fill: self.colors.secondary, (0pt, 0pt), (50pt, 0pt), (0pt, 50pt)))
    place(bottom+right, polygon(fill: self.colors.secondary, (50pt, 50pt), (50pt, 0pt), (0pt, 50pt)))

    align(bottom, block(text(size: .4em, note), width: 80%))
  }
  touying-slide(self: self, body)
})

#let new-section-slide(self: none, note) = touying-slide-wrapper(self => {
  let main-body = {
    set align(center + horizon)

    smallcaps(strong(utils.display-current-heading(level: 1)))
    block(line(start: (0pt, 0pt), length: 100%, stroke: self.colors.tertiary), width: 20%, above: 0.8em, below: 0.3em)

    place(bottom+right, polygon(fill: self.colors.secondary, (0pt, 0pt), (50pt, 0pt), (0pt, 50pt)))
    place(top+left, polygon(fill: self.colors.secondary, (50pt, 50pt), (50pt, 0pt), (0pt, 50pt)))

    align(bottom, block(text(size: .4em, note), width: 80%))
  }
    
  touying-slide(self: self, main-body)
})

/// My custom theme for Touying designed for making coding related presentations. Requires the #link("https://github.com/githubnext/monaspace?tab=readme-ov-file#monaspace")[Monaspace] font to be installed.
///
///```typst
///#import code-theme: *
///
///#show: code-theme```
///
/// This template includes a shortcut for pdfpc notes. You can use `note` as the language in a raw block and it will be treated as a speaker note:
///
/// ````typst
/// ```note
/// # What a wonderful markdown note!
/// Mention that you are the yeast of thoughts and mind
/// ```
/// ````
#let code-theme(
  /// The presentation's aspect ratio -> string
  aspect-ratio: "16-9",
  palette: (
    background: rgb("#180b20"),
    text: white,
    primary: rgb("#823372"),
    accent: rgb("#df5671"),
  ),
  body,
) = {
  show: touying-slides.with(
    config-page(
      paper: "presentation-" + aspect-ratio,
      fill: palette.background,
      margin: 1em,
    ),
    config-common(
      slide-fn: slide,
      title-slide-fn: title-slide,
      new-section-slide-fn: new-section-slide,
    ),
    config-methods(
      init: (self: none, body) => {
        set list(marker: ([#sym.bullet], [--]))

        set text(fill: self.colors.primary, size: 40pt, font: "Monaspace Neon Var")

        set raw(theme: "Tomorrow-Night-Bright.tmTheme")

        show raw: it => {
          if it.lang == "note" {
            return pdfpc.speaker-note(it)
          }  

          show-code(it)
        }

        body
      }
    ),
    config-colors(
      neutral: palette.background,
      primary: palette.text,
      secondary: palette.primary,
      tertiary: palette.accent,
    ),
    config-store(
      title: none,
      footer: none,
    ),
  ) 

  body
}

